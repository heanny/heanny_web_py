from flask_httpauth import HTTPTokenAuth
from flask_sqlalchemy import SQLAlchemy
db = SQLAlchemy(use_native_unicode='utf8')
auth = HTTPTokenAuth()
