from flask import Flask

from exts import db
from flask_cors import *

app = Flask(__name__, template_folder='./templates', static_url_path='', static_folder='./static')
app.config.from_object('config')

db.init_app(app)
'''默认对象转json'''

CORS(app, supports_credentials=True)  # 设置跨域

from . import models, views
# from app.Bookkeeping import Bpage
# from app.HeannyBlog import Hpage
# from app.LoveMeizhi import MZpage
# from app.Notes import Notepage
# from app.NAS import Npage
# from app.api import Api

# app.register_blueprint(Bpage, url_prefix='/Bookkeeping/api')
# app.register_blueprint(Hpage, url_prefix='/Heannyblog/api')
# app.register_blueprint(Notepage, url_prefix='/HeannyNote/api')
# app.register_blueprint(MZpage, url_prefix='/LoveMeizhi/api')
# app.register_blueprint(Npage, url_prefix='/NAS/api')
# app.register_blueprint(Api, url_prefix='/api')
