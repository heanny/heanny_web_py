from exts import db


class heannyPost(db.Model):
    __tablename__ = 'new_posts'
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(255), unique=False)
    intro = db.Column(db.Text, unique=False)
    content = db.Column(db.Text, unique=False)
    img = db.Column(db.String(255), unique=False)
    tags = db.Column(db.String(255), unique=False)
    time = db.Column(db.String(255), unique=False)
    user = db.Column(db.String(255), unique=False)
    encryption = db.Column(db.Integer, unique=False)
    top = db.Column(db.Integer, unique=False)
    view = db.Column(db.Integer, unique=False)
    en_pwd = db.Column(db.String(255), unique=False)

    def to_dict(self):
        '''将对象转换为字典数据'''
        reDict = {
            "id": self.id,
            "title": self.title,
            "intro": self.intro if int(self.encryption) == 0 else '已加密',
            "content": self.content.replace('//www.heanny.cn/update', 'http://cdn.heanny.cn') if int(
                self.encryption) == 0 else '已加密',
            "img": self.img,
            "tags": self.tags.split(','),
            "time": self.time,
            "user": self.user,
            "encryption": self.encryption,
            "top": self.top,
            "view": self.view,
            "en_pwd": self.en_pwd,

        }
        return reDict

    def getPost(self):
        '''将对象转换为字典数据'''
        reDict = {
            "id": self.id,
            "title": self.title,
            "intro": self.intro if int(self.encryption) == 0 else '已加密',
            "img": self.img,
            "tags": self.tags.split(','),
            "time": self.time,
            "user": self.user,
            "encryption": self.encryption,
            "top": self.top,
            "view": self.view,
            "en_pwd": self.en_pwd,

        }
        return reDict


class heannyPhoto(db.Model):
    __tablename__ = 'new_photos'
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(255), unique=False)
    imgurl = db.Column(db.String(255), unique=False)
    time = db.Column(db.String(255), unique=False)
    user = db.Column(db.String(255), unique=False)
    show = db.Column(db.Integer, unique=False)

    def to_dict(self):
        '''将对象转换为字典数据'''
        reDict = {
            "id": self.id,
            "title": self.title,
            "imgurl": self.imgurl,
            "time": self.time,
            "user": self.user,
            "show": self.show,
        }
        return reDict

class heannyLink(db.Model):
    __tablename__ = 'new_links'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(255), unique=False)
    mail = db.Column(db.String(255), unique=False)
    webname = db.Column(db.String(255), unique=False)
    weburl = db.Column(db.String(255), unique=False)
    time = db.Column(db.String(255), unique=False)
    bool = db.Column(db.Integer, unique=False)
    text = db.Column(db.String(255), unique=False)
    ip = db.Column(db.String(255), unique=False)

    def to_dict(self):
        '''将对象转换为字典数据'''
        reDict = {
            "id": self.id,
            "name": self.name,
            "mail": self.mail,
            "webname": self.webname,
            "weburl": self.weburl,
            "time": self.time,
            "bool": self.bool,
            "text": self.text,
            "ip": self.ip,
        }
        return reDict

class heannyTimeline(db.Model):
    __tablename__ = 'new_timeline'
    id = db.Column(db.Integer, primary_key=True)
    icon = db.Column(db.String(255), unique=False)
    title = db.Column(db.String(255), unique=False)
    text = db.Column(db.String(255), unique=False)
    date = db.Column(db.String(255), unique=False)

    def to_dict(self):
        '''将对象转换为字典数据'''
        reDict = {
            "id": self.id,
            "icon": self.icon,
            "title": self.title,
            "text": self.text,
            "date": self.date,
        }
        return reDict


class heannyGbook(db.Model):
    __tablename__ = 'new_gbooks'
    id = db.Column(db.Integer, primary_key=True)
    text = db.Column(db.String(255), unique=False)
    mail = db.Column(db.String(255), unique=False)
    time = db.Column(db.String(255), unique=False)
    user = db.Column(db.String(255), unique=False)
    show = db.Column(db.Integer, unique=False)
    ip = db.Column(db.String(255), unique=False)

    def to_dict(self):
        '''将对象转换为字典数据'''
        reDict = {
            "id": self.id,
            "text": self.text,
            "mail": self.mail,
            "time": self.time,
            "user": self.user,
            "show": self.show,
            "ip": self.ip,
            "reply": list(map(lambda x: x.to_dict(), heannyGbookReply.query.filter_by(gid=self.id).all()))
        }
        return reDict


class heannyGbookReply(db.Model):
    __tablename__ = 'new_gbook_reply'
    id = db.Column(db.Integer, primary_key=True)
    gid = db.Column(db.Integer, unique=False)
    time = db.Column(db.String(255), unique=False)
    text = db.Column(db.String(255), unique=False)

    def to_dict(self):
        '''将对象转换为字典数据'''
        reDict = {
            "id": self.id,
            "text": self.text,
            "gid": self.gid,
            "time": self.time,
        }
        return reDict


class heanny_qr_code(db.Model):
    __tablename__ = 'heanny_qr_code'
    code = db.Column(db.String(255), primary_key=True)
    state = db.Column(db.Integer, unique=False)
    nickName = db.Column(db.String(255), unique=False)
    openid = db.Column(db.String(255), unique=False)
    avatarUrl = db.Column(db.String(255), unique=False)

    def to_dict(self):
        '''将对象转换为字典数据'''
        reDict = {
            "code": self.code,
            "state": self.state,
            "nickName": self.nickName,
            "openid": self.openid,
            "avatarUrl": self.avatarUrl,
        }
        return reDict
