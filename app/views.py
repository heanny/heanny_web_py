import calendar
import datetime
import hashlib
import math, requests, random
import os, platform, random, re, time, paramiko, json, logging
from functools import wraps

from flask import jsonify, redirect, request, Flask, render_template, g, make_response, current_app, url_for, session
from flask_httpauth import HTTPBasicAuth
from pyexcel_xlsx import get_data
from sqlalchemy import desc, asc, func
from werkzeug.utils import secure_filename
from werkzeug.wrappers import Response
from exts import db, auth
# models

from .models import *
from itsdangerous import TimedJSONWebSignatureSerializer as Serializer
from app import app, db
# from app.LoveMeizhi import index as MZindex

@app.route('/')
def index():
    return 'hello world'

@app.route('/mz')
def MZIndex():
    # return redirect(url_for('/LoveMeizhi/api'))
    # return url_for('MZindex')
    return redirect('/LoveMeizhi/api')
@app.route('/nas/')
@app.route('/NAS/')
@app.route('/NAS/index.html')
@app.route('/nas/index.html')
def NASIndex():
    # return redirect(url_for('/LoveMeizhi/api'))
    # return url_for('MZindex')
    return render_template('index.html', **locals())

